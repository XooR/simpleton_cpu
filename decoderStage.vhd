library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;

entity decoderStage is
  port (
    clk : in bit;
    -- instruction register 32 bits
    ir  : in word;

    -- Content of rs1,rs2 regs, or zero if non existant
    rs1 : out word;
    rs2 : out word;
    -- So we now what to do in Ex stage
    opc    : out opcode;
    imm16  : out imm16t;

    coin_bit_in  : in bit;
    coin_bit_out : out bit;
    reset_cpu    : in bit;

    -- To write reg
    wr       : in bit;
    rwr16    : in bit;
    rwr_data : in word;

    halt     : out bit
  );
end decoderStage;

architecture Behavioral of decoderStage is
     signal     rd            : reg_addr;
     signal     rs2_adr       : reg_addr;
     signal     rs1_or_rd_adr : reg_addr;
     signal     coin_bit_out_buff: bit;
begin
  coin1: reg1 port map(
    input         => coin_bit_in,
    output        => coin_bit_out_buff,
    clk           => clk,
    default_value => '0',
    cl            => reset_cpu,
    ld            => '1'
  );
  coin_bit_out <= coin_bit_out_buff;
  registers: registerFile port map(
      clk          => clk,
      read_allowed => coin_bit_out_buff,
      rs1          => rs1_or_rd_adr,
      rs1_data     => rs1,
      rs2          => rs2_adr,
      rs2_data     => rs2,
      cl           => reset_cpu,
-- should we write
      wr       => wr,
-- address of reg that we should write to
      rwr      => rd,
-- do we take only 16 lower bits
      rwr16    => rwr16,
      rwr_data => rwr_data
  );
  
  main: process(ir)
   variable imm16_tmp   : imm16t;
   variable opc_tmp     : opcode;
   variable rd_tmp      : reg_addr;
   variable tmp_rs1_adr : reg_addr;
   variable tmp_rs2_adr : reg_addr;
  begin
    -- init all to zero, than fill stuff that are needed
    imm16_tmp := (others => '0');

    opc_tmp(5 downto 0) := ir(5 downto 0);
    opc                 <= opc_tmp;
    
    -- So when we use "int(rd)" we get right number
    rd_tmp(0) := ir(10);
    rd_tmp(1) := ir(9);
    rd_tmp(2) := ir(8);
    rd_tmp(3) := ir(7);
    rd_tmp(4) := ir(6);
    rd        <= rd_tmp;

    tmp_rs1_adr(0) := ir(15);
    tmp_rs1_adr(1) := ir(14);
    tmp_rs1_adr(2) := ir(13);
    tmp_rs1_adr(3) := ir(12);
    tmp_rs1_adr(4) := ir(11);
    rs1_or_rd_adr <= tmp_rs1_adr; -- by default, only not true on shift ops

    tmp_rs2_adr(0) := ir(20);
    tmp_rs2_adr(1) := ir(19);
    tmp_rs2_adr(2) := ir(18);
    tmp_rs2_adr(3) := ir(17);
    tmp_rs2_adr(4) := ir(16);
    rs2_adr <= tmp_rs2_adr;

    halt <= '0';
    case opc_tmp is
      when OP_LOAD | OP_ADDI | OP_ADD | OP_MOVI | OP_JSR | OP_JMP =>
        -- Can't do this
        -- imm16_tmp(15 downto 0) := ir(16 to 31);
        imm16_tmp(0) := ir(31);
        imm16_tmp(1) := ir(30);
        imm16_tmp(2) := ir(29);
        imm16_tmp(3) := ir(28);
        imm16_tmp(4) := ir(27);
        imm16_tmp(5) := ir(26);
        imm16_tmp(6) := ir(25);
        imm16_tmp(7) := ir(24);
        imm16_tmp(8) := ir(23);
        imm16_tmp(9) := ir(22);
        imm16_tmp(10) := ir(21);
        imm16_tmp(11) := ir(20);
        imm16_tmp(12) := ir(19);
        imm16_tmp(13) := ir(18);
        imm16_tmp(14) := ir(17);
        imm16_tmp(15) := ir(16);

      when OP_STORE | OP_BEQ | OP_BNQ | OP_BLE | OP_BLT | OP_BGT | OP_BGE =>
        imm16_tmp(0)  := ir(31);
        imm16_tmp(1)  := ir(30);
        imm16_tmp(2)  := ir(29);
        imm16_tmp(3)  := ir(28);
        imm16_tmp(4)  := ir(27);
        imm16_tmp(5)  := ir(26);
        imm16_tmp(6)  := ir(25);
        imm16_tmp(7)  := ir(24);
        imm16_tmp(8)  := ir(23);
        imm16_tmp(9)  := ir(22);
        imm16_tmp(10) := ir(21);

        imm16_tmp(11) := ir(10);
        imm16_tmp(12) := ir(9);
        imm16_tmp(13) := ir(8);
        imm16_tmp(14) := ir(7);
        imm16_tmp(15) := ir(6);

      when OP_SHL | OP_SHR | OP_SAR | OP_ROL | OP_ROR =>
        imm16_tmp(0) := ir(20);
        imm16_tmp(1) := ir(19);
        imm16_tmp(2) := ir(18);
        imm16_tmp(3) := ir(17);
        imm16_tmp(4) := ir(16);

        rs1_or_rd_adr <= rd_tmp;
      when OP_HALT =>
        report "We detected HALT OP";
        halt <= '1';
      when others =>
        imm16_tmp := (others => '0');
    end case;

    imm16 <= imm16_tmp;
  end process;

end Behavioral;

