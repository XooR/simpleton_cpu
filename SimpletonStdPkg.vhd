library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

package SimpletonStdPkg is

  constant OPCODE_LEN    : integer := 6;
  constant WORD_LEN      : integer := 32;
  constant REG_ADR_LEN   : integer := 5;
  constant NUM_OF_STAGES : integer := 3;

  -- For outside address and data
  subtype sword     is std_logic_vector (WORD_LEN-1     downto 0);

  subtype opcode    is bit_vector (OPCODE_LEN-1   downto 0);
  subtype word      is bit_vector (WORD_LEN-1     downto 0);
  subtype word_ext  is bit_vector (WORD_LEN       downto 0);
  subtype reg_addr  is bit_vector (REG_ADR_LEN-1  downto 0);
  subtype imm16t    is bit_vector(15 downto 0);

  -- stack
  constant OP_PUSH    : opcode := "001001";
  constant OP_POP     : opcode := "101001";
  constant OP_JSR     : opcode := "100001";
  constant OP_RTS     : opcode := "010001";
  -- rotate/shift
  constant OP_ROL     : opcode := "110110";
  constant OP_SHL     : opcode := "000110";
  constant OP_ROR     : opcode := "001110";
  constant OP_SAR     : opcode := "010110";
  constant OP_SHR     : opcode := "100110";
  -- JMP/BRANCH
  constant OP_JMP     : opcode := "000001";
  constant OP_BGE     : opcode := "001101";
  constant OP_BEQ     : opcode := "000101";
  constant OP_BGT     : opcode := "010101";
  constant OP_BNQ     : opcode := "100101";
  constant OP_BLE     : opcode := "101101";
  constant OP_BLT     : opcode := "110101";
  -- MEM
  constant OP_STORE   : opcode := "100000";
  constant OP_LOAD    : opcode := "000000";
  -- Arith/BIT
  constant OP_SUB     : opcode := "100100";
  constant OP_ADD     : opcode := "000100";
  constant OP_SUBI    : opcode := "101100";
  constant OP_ADDI    : opcode := "001100";
  constant OP_OR      : opcode := "100010";
  constant OP_XOR     : opcode := "010010";
  constant OP_AND     : opcode := "000010";
  constant OP_NOT     : opcode := "110010";
  -- MOV
  constant OP_MOV     : opcode := "001000";
  constant OP_MOVI    : opcode := "101000";

  constant OP_HALT    : opcode := "000011";
  constant OP_NOP     : opcode := "111111";

  component CPU
  port (
    addr  : out   sword;
    data  : inout sword;
    r     : out std_logic;
    w     : out std_logic;
    clk   : in  bit;
    reset : in bit;
    halt  : out bit
  );
  end component;
  component ifStage
  port (
    addr            : out sword;
    data            : in  sword;
    r               : out std_logic;
    w               : out std_logic;
    clk             : in  bit;
    ir              : out word;
    coin_bit_in     : in bit;
    coin_bit_out    : out bit;
    branch_pc       : in word;
    branch_pc_valid : in bit;
    current_pc_out  : out word;
    reset_cpu       : in bit
  );
  end component;
  component decoderStage
    port (
      clk          : in bit;
      ir           : in word;
      rs1          : out word;
      rs2          : out word;
      opc          : out opcode;
      imm16        : out imm16t;
      coin_bit_in  : in bit;
      coin_bit_out : out bit;
      reset_cpu    : in bit;
      wr           : in bit;
      rwr16        : in bit;
      rwr_data     : in word;
      halt         : out bit
    );
  end component;
  component exMemStage
    port (
      addr            : out   sword;
      data            : inout sword;
      r               : out std_logic;
      w               : out std_logic;
      clk             : in bit;
      rs1             : in word;
      rs2             : in word;
      opc             : in opcode;
      imm16           : in imm16t;
      coin_bit_in     : in bit;
      coin_bit_out    : out bit;
      reset_cpu       : in bit;
      wr_reg          : out bit;
      rwr16           : out bit;
      wr_reg_data     : out word;
      branch_pc_valid : out bit;
      branch_pc       : out word;
      current_pc      : in word
    );
  end component;
  component spReg32
  port (
    input  : in word;
    output : out word;
    clk    : in bit;
    inc    : in bit;
    dec    : in bit;
    ld     : in bit;
    cl     : in bit
   );
  end component;
  component decrement_32
  port (
    input  : in word;
    output : out word;
    c      : out bit
  );
  end component;

  component exALU
    port (
      op        : in opcode;
      rs1       : in word;
      rs2       : in word;
      alu_out   : out word;
      calc_cond : in bit;
      v         : out bit;
      c         : out bit;
      z         : out bit;
      n         : out bit
    );
  end component;

  component registerFile
    port (
      clk          : in  bit;
      read_allowed : in  bit;
      rs1          : in  reg_addr;
      rs1_data     : out word;
      rs2          : in  reg_addr;
      rs2_data     : out word;
      cl           : in  bit;
      wr           : in  bit;
      rwr          : in  reg_addr;
      rwr16        : in  bit;
      rwr_data     : in  word
    );
  end component;

  component reg32 is
    port (
      input  : in word;
      output : out word;
      clk    : in bit;
      ld     : in bit;
      cl     : in bit
     );
  end component;

  component reg1 is
    port (
      input         : in bit;
      output        : out bit;
      clk           : in bit;
      default_value : in bit;
      cl            : in bit;
      ld            : in bit
    );
  end component;

  component mux2_32 is
    port (
      first: in word;
      second: in word;
      select_second: in bit;
      selected_word: out word
    );
  end component;

  component increment_32 is
    port (
      input: in word;
      output: out word;
      c: out bit
    );
  end component;

  component bit_pass32 is
    port (
      pass   : in bit;
      input  : in word;
      output : out sword
    );
  end component;

  component std_pass32 is
    port (
      pass   : in bit;
      input  : in sword;
      output : out word
    );
  end component;

  function rising_edge4bit (signal s: bit) return boolean;
  function falling_edge4bit(signal s: bit) return boolean;
  function to_std_bit (s: bit) return std_logic;
  FUNCTION vec2str(vec : bit_vector) RETURN string;
end SimpletonStdPkg;

package body SimpletonStdPkg is
  function rising_edge4bit(signal s: bit) return boolean is
  begin
      return (s'event and (s = '1') and
                          (s'last_value = '0'));
  end;

  function falling_edge4bit(signal s: bit) return boolean is
  begin
      return (s'event and (s = '0') and
                          (s'last_value = '1'));
  end;

  function to_std_bit (s: bit) return std_logic is
  begin
    if s = '1' then
      return '1';
    else
      return '0';
    end if;
  end;

-- function to convert vector to string
-- for use in assert statements
  FUNCTION vec2str(vec : bit_vector) RETURN string IS
    VARIABLE stmp : string(vec'LEFT+1 DOWNTO 1);
  BEGIN
    FOR i IN vec'REVERSE_RANGE LOOP
      IF vec(i) = '1' THEN
        stmp(i+1) := '1';
      ELSE
        stmp(i+1) := '0';
      END IF;
    END LOOP;
    RETURN stmp;
  END vec2str;
  
end SimpletonStdPkg;
