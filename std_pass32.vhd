library ieee;
use ieee.numeric_bit.all;
use ieee.std_logic_1164.all;

use work.SimpletonStdPkg.all;

-- It pass value from std_logic to bit_vector, if pass is 0, then it returns 0
entity std_pass32 is
  port (
    pass   : in bit;
    input  : in sword;
    output : out word
  );
end std_pass32;

architecture Behavioral of std_pass32 is
begin
  process (pass, input)
  begin
    if pass = '1' then
      output <= To_bitvector(input);
    else
      output <= (others => '0');
    end if;
  end process;


end Behavioral;

