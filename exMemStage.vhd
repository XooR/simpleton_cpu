library ieee;
use ieee.numeric_bit.all;
use ieee.std_logic_1164.all;

use work.SimpletonStdPkg.all;

entity exMemStage is
  port (
    addr : out   sword;
    data : inout sword;
    r    : out std_logic;
    w    : out std_logic;

    clk          : in bit;
    rs1          : in word;
    rs2          : in word;
    opc          : in opcode;
    imm16        : in imm16t;
    coin_bit_in  : in bit;
    coin_bit_out : out bit;
    reset_cpu    : in bit;

    -- wr decoderStage, should we write it
    wr_reg      : out bit;
    -- write only 16 lower bits
    rwr16       : out bit;
    wr_reg_data : out word;

    branch_pc_valid : out bit;
    branch_pc       : out word;
    -- from IR
    current_pc      : in word
  );
end exMemStage;

architecture Behavioral of exMemStage is
  -- true when this stage is selected
  signal working: bit;
  -- ALU out
  signal v       : bit;
  signal c       : bit;
  signal z       : bit;
  signal n       : bit;
  signal alu_out : word;

  -- coins that count how much clks we have on this stage
  signal coin_in_middle    : bit;
  signal coin_bit_out_buff : bit;

  -- This are two mux before ALU unit
  signal choose_pc_instead_rs1   : bit;
  signal rs1_or_pc               : word;
  signal choose_imm_instead_rs2  : bit;
  signal rs2_or_imm              : word;

  -- This is before REG stage, so we choose if we will take data
  -- from ALU unit or data mag
  signal choose_data_instead_alu : bit;
  -- we convert std_logic data -> bit_vector
  signal data_bitv               : word;

  -- Should we take ALU result as addr
  signal pass_alu_to_addr        : bit;
  -- in case of STORE instruction
  signal pass_rs2_to_data        : bit;

  -- We take imm16 and do sign extension and we get imm
  signal imm                     : word;

  signal reset_coin1_output : bit;
  signal reset_coin2_output : bit;

  -- CND stuff
  signal branch_successfull : bit;
  signal calc_cond          : bit;

  -- SP stuff
  -- SP reg
  signal currentSP             : word;
  signal incSP                 : bit;
  signal decSP                 : bit;
  -- ADDR
  signal decCurrentSP          : word;
  -- From mux between currentPC and decCurrentSP
  signal SP_for_addr           : word;
  signal pass_SP_to_addr       : bit;
  -- NOT needed
  signal SPc                   : bit;
  -- PUSH
  signal pass_rs1_to_data      : bit;
  signal choose_dec_current_sp : bit;
  -- JSR, save PC
  signal pass_pc_to_data       : bit;
  -- RTS, recover old PC
  signal alu_or_data             : word;
begin
  --
  -- WORKING
  --
  -- We have two clocks for this stage, think this is enough for mem operations
  coin1: reg1 port map(
    input         => coin_bit_in,
    output        => coin_in_middle,
    clk           => clk,
    default_value => '0',
    cl            => reset_cpu,
    ld            => '1'
  );
  coin2: reg1 port map(
    input         => coin_in_middle,
    output        => coin_bit_out_buff,
    clk           => clk,
    default_value => '0',
    cl            => reset_cpu,
    ld            => '1'
  );
  coin_bit_out <= coin_bit_out_buff;
  working      <= coin_bit_out_buff or coin_in_middle;

  --
  -- NEXT PC Register
  --
  alu_data_mux: mux2_32 port map(
    first         => alu_out,
    second        => data_bitv,
    select_second => choose_data_instead_alu,

    selected_word => alu_or_data
  );
  next_pc: reg32 port map(
      input  => alu_or_data,
      output => branch_pc,
      clk    => clk,
      ld     => working,
      cl     => reset_cpu
  );

  -- So we have two clocks branch_pc_valid after reset, so PC be 0000h in
  -- first pass
  reset_coin1: reg1 port map(
    input         => '0',
    output        => reset_coin1_output,
    clk           => clk,
    default_value => '1',
    cl            => reset_cpu,
    ld            => '1'
  );
  reset_coin2: reg1 port map(
    input         => reset_coin1_output,
    output        => reset_coin2_output,
    clk           => clk,
    default_value => '0',
    cl            => reset_cpu,
    ld            => '1'
  );

  -- If branch is successfull

  reg_branch_successfull: reg1 port map(
    input         => alu_out(0),
    output        => branch_successfull,
    clk           => clk,
    default_value => '0',
    cl            => reset_cpu,
    -- In first cycle we decide if we want to jump
    -- in second we calc next address
    ld            => coin_in_middle
  );

  --
  -- ALU
  --
  rs1_or_pc_mux: mux2_32 port map(
    first         => rs1,
    second        => current_pc,
    select_second => choose_pc_instead_rs1,

    selected_word => rs1_or_pc
  );

  -- extended imm operand with 16bits based on sign
  rs2_or_imm_mux: mux2_32 port map(
    first         => rs2,
    second        => imm,
    select_second => choose_imm_instead_rs2,

    selected_word => rs2_or_imm
  );
  process (imm16)
    variable imm_tmp : word;
  begin
    if imm16(imm16'left) = '0' then
      imm_tmp := X"0000" & imm16;
    else
      imm_tmp := X"FFFF" & imm16;
    end if;

    imm <= imm_tmp;
  end process;
  pass_rs2_to_datai: bit_pass32 port map(
    input  => rs2,
    output => data,
    pass   => pass_rs2_to_data
  );

  exALUi: exALU port map(
    op        => opc,
    rs1       => rs1_or_pc,
    rs2       => rs2_or_imm,
    calc_cond => calc_cond,
    alu_out   => alu_out,
    v         => v,
    c         => c,
    z         => z,
    n         => n
  );

  pass_alu_to_addri: bit_pass32 port map(
    input  => alu_out,
    output => addr,
    pass   => pass_alu_to_addr
  );

  --
  -- SP
  --
  sp: spReg32 port map (
    -- FIXME: Taken from test[CPU], MEM is 300 size
    -- 300 because SP points to last value put on stack, it will be decremented
    -- before push, pop is invalid when stack is empty
    input  => B"0000_0000_0000_0000_0000_0001_0010_1100", -- 300(10)
    output => currentSP,
    clk    => clk,
    inc    => incSP,
    dec    => decSP,
    ld     => reset_cpu,
    cl     => '0'
  );
  decSP32: decrement_32 port map (
      input  => currentSP,
      output => decCurrentSP,
      c      => SPc
  );
  SP_current_dec_mux: mux2_32 port map(
    first         => currentSP,
    second        => decCurrentSP,
    select_second => choose_dec_current_sp,

    selected_word => SP_for_addr
  );
  pass_sp_to_addri: bit_pass32 port map(
    input  => SP_for_addr,
    output => addr,
    pass   => pass_SP_to_addr
  );
  pass_rs1_to_datai: bit_pass32 port map(
    input  => rs1,
    output => data,
    pass   => pass_rs1_to_data
  );
  pass_pc_to_datai: bit_pass32 port map(
    input  => current_pc,
    output => data,
    pass   => pass_pc_to_data
  );
  --
  -- REG DATA
  --
  data_bit_conv: std_pass32 port map(
    input  => data,
    output => data_bitv,
    pass   => '1'
  );
  data_alu_mux: mux2_32 port map(
    first         => alu_out,
    second        => data_bitv,
    select_second => choose_data_instead_alu,

    selected_word => wr_reg_data
  );
  -- add next mux for stack

  process (opc, working, reset_cpu, reset_coin1_output, reset_coin2_output, coin_in_middle, coin_bit_out_buff)
    variable tmp_calc_cond : bit;
  begin
    -- we choose default values, so we have less case options
    pass_alu_to_addr        <= '0';
    pass_rs2_to_data        <= '0';
    choose_data_instead_alu <= '0';
    choose_imm_instead_rs2  <= '0';
    choose_pc_instead_rs1   <= '0';
    wr_reg                  <= '0';
    r                       <= 'Z';
    w                       <= 'Z';
    tmp_calc_cond           := '0';

    -- SP
    incSP                   <= '0';
    decSP                   <= '0';
    pass_SP_to_addr         <= '0';
    pass_rs1_to_data        <= '0';
    choose_dec_current_sp   <= '0';
    choose_data_instead_alu <= '0';
    -- JSR
    pass_pc_to_data         <= '0';

    -- NOTE: Every OP case need to have branch_pc_valid <= '0'
    -- If it don't produce branch op, this is because OP_B** have if/else and
    -- branch_pc_valid get's reset as soon we get out EXMEM stage
    if working = '1' then
      case opc is
        when OP_LOAD  =>
          branch_pc_valid         <= '0';
          -- rd <= MEM[rs1 + imm]
          pass_alu_to_addr        <= working;
          choose_imm_instead_rs2  <= '1';
          wr_reg                  <= working;
          choose_data_instead_alu <= working;
          r                       <= to_std_bit(working);

        -- MEM[rs1 + imm] = rs2
        when OP_STORE =>
          branch_pc_valid        <= '0';
          pass_alu_to_addr       <= '1';
          choose_imm_instead_rs2 <= '1';

          pass_rs2_to_data       <= '1';
          w                      <= to_std_bit(working);

        when OP_BEQ | OP_BNQ | OP_BLE | OP_BLT | OP_BGT | OP_BGE =>
          if coin_in_middle = '1' then
            tmp_calc_cond := '1'; -- ALU calc condition -> branch_successfull
          elsif coin_bit_out_buff = '1' then
            choose_pc_instead_rs1  <= '1';
            choose_imm_instead_rs2 <= '1';
            branch_pc_valid        <= branch_successfull;
          end if;

        -- here rs1 have value of rd reg
        when OP_MOVI | OP_SHL | OP_SHR | OP_SAR | OP_ROL | OP_ROR =>
          branch_pc_valid        <= '0';
          choose_imm_instead_rs2 <= '1';
          wr_reg                 <= working;

        when OP_PUSH =>
          branch_pc_valid       <= '0';
          choose_dec_current_sp <= '1';
          pass_SP_to_addr       <= working;
          pass_rs1_to_data      <= working;
          w                     <= to_std_bit(working);
          decSP                 <= coin_bit_out_buff;
        when OP_POP =>
          branch_pc_valid         <= '0';
          choose_dec_current_sp   <= '0';
          pass_SP_to_addr         <= working;
          r                       <= to_std_bit(working);
          incSP                   <= coin_bit_out_buff;
          wr_reg                  <= working;
          choose_data_instead_alu <= working;
        when OP_JSR =>
          branch_pc_valid        <= '1';
          -- rs1 + imm -> branch_pc
          choose_imm_instead_rs2 <= '1';

          -- Addr
          choose_dec_current_sp <= '1';
          pass_SP_to_addr       <= working;
          decSP                 <= coin_bit_out_buff;
          -- Data
          pass_pc_to_data <= working;
          w               <= to_std_bit(working);
        when OP_JMP =>
          branch_pc_valid        <= '1';
          -- rs1 + imm -> branch_pc
          choose_imm_instead_rs2 <= '1';
        when OP_RTS =>
          branch_pc_valid         <= '1';
          choose_dec_current_sp   <= '0';

          -- Addr
          pass_SP_to_addr         <= working;
          incSP                   <= coin_bit_out_buff;
          -- Data
          r                       <= to_std_bit(working);
          choose_data_instead_alu <= '1'; -- -> branchPC
        when others =>
          branch_pc_valid <= '0';
          wr_reg          <= working;

      end case;
    end if;
    -- branch_pc_valid needs to be valid at first two clocks after reset so we
    -- read MEM[0] -> IR
    if reset_coin1_output = '1' or reset_coin2_output = '1' then
      branch_pc_valid <= '1';
    end if;
    calc_cond       <= tmp_calc_cond;
  end process;

end Behavioral;
