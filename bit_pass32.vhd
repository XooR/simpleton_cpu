library ieee;
use ieee.numeric_bit.all;
use ieee.std_logic_1164.all;

use work.SimpletonStdPkg.all;

-- We use this entity to pass bit values to magistrall
entity bit_pass32 is
  port (
    pass   : in bit;
    input  : in word;
    output : out sword
  );
end bit_pass32;

architecture Behavioral of bit_pass32 is

begin
  process (pass, input)
    variable output_value: sword;
  begin
    if pass = '1' then
      output_value := To_StdLogicVector(input);
    else
      output_value := (others => 'Z');
    end if;
    output <= output_value;
  end process;

end Behavioral;

