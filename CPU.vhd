library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;

entity CPU is
  port (
    addr  : out   sword;
    data  : inout sword;
    r     : out std_logic;
    w     : out std_logic;
    clk   : in  bit;
    reset : in bit;
    halt  : out bit
  );
end CPU;

architecture Behavioral of CPU is
  signal coin_between_ex_if_stage  : bit;
  signal coin_between_if_id_stage  : bit;
  signal coin_between_id_mem_stage : bit;

  -- if
  signal ir              : word;
  signal branch_pc       : word;
  signal branch_pc_valid : bit;
  signal current_pc      : word;

  -- id
  signal rs1      : word;
  signal rs2      : word;
  signal opc      : opcode;
  signal imm16    : imm16t;
  signal wr       : bit;
  signal rwr16    : bit;
  signal rwr_data : word;
begin
  ifStagei: ifStage port map
  (
    addr            => addr,
    data            => data,
    r               => r,
    w               => w,
    clk             => clk,
    ir              => ir,
    coin_bit_in     => coin_between_ex_if_stage,
    coin_bit_out    => coin_between_if_id_stage,
    branch_pc       => branch_pc,
    branch_pc_valid => branch_pc_valid,
    current_pc_out  => current_pc,
    reset_cpu       => reset
  );
  decoderStagei: decoderStage port map
  (
    clk          => clk,
    ir           => ir,
    rs1          => rs1,
    rs2          => rs2,
    opc          => opc,
    imm16        => imm16,
    coin_bit_in  => coin_between_if_id_stage,
    coin_bit_out => coin_between_id_mem_stage,
    reset_cpu    => reset,
    wr           => wr,
    rwr16        => rwr16,
    rwr_data     => rwr_data,
    halt         => halt
  );
  exMemStagei: exMemStage port map
  (
    addr            => addr,
    data            => data,
    r               => r,
    w               => w,
    clk             => clk,
    rs1             => rs1,
    rs2             => rs2,
    opc             => opc,
    imm16           => imm16,
    coin_bit_in     => coin_between_id_mem_stage,
    coin_bit_out    => coin_between_ex_if_stage,
    reset_cpu       => reset,
    wr_reg          => wr,
    rwr16           => rwr16,
    wr_reg_data     => rwr_data,
    branch_pc_valid => branch_pc_valid,
    branch_pc       => branch_pc,
    current_pc      => current_pc
  );

end Behavioral;

