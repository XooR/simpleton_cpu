library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;

entity ifStage is
  port (
    -- External connections
    addr : out sword;
    data : in  sword;
    r    : out std_logic;
    w    : out std_logic;

    clk  : in bit;

    -- instruction that is read -> decoderStage
    ir: out word;
    -- we transfer this coin through all three stages, and when it comes back
    -- we can read next instruction word
    coin_bit_in  : in bit;
    coin_bit_out : out bit;

    -- PC that is calculated by executionStage
    branch_pc       : in word;
    -- '1' - yes, '0' - no
    branch_pc_valid : in bit;
    -- so we could calc new one in exMemStage
    current_pc_out  : out word;

    -- with this we reset whole cpu :)
    reset_cpu: in bit
);
end ifStage;

architecture Behavioral of ifStage is
  signal working             : bit;
  signal coin_bit_out_buff   : bit;
  signal coin_bit_out_middle : bit;
  -- from mux -> PC
  signal selected_pc : word;
  -- what was remembered in PC
  signal current_pc     : word;
  -- and incremented after
  signal inc_current_pc : word;
  signal inc_pc_c_bit    : bit;

  signal data_bitv : word;
begin

  w <= 'Z';

  -- coin1 is only '1' default, for reset
  coin1: reg1 port map(
    input         => coin_bit_in,
    output        => coin_bit_out_middle,
    clk           => clk,
    default_value => '1',
    cl            => reset_cpu,
    ld            => '1'
  );
  coin2: reg1 port map(
    input         => coin_bit_out_middle,
    output        => coin_bit_out_buff,
    clk           => clk,
    default_value => '0',
    cl            => reset_cpu,
    ld            => '1'
  );
  -- we are working two clock cycles
  working  <= coin_bit_out_middle or coin_bit_out_buff;

  read_signal: process (working)
  begin
    if working = '1' then
      r <= '1';
    else
      r <= 'Z';
    end if;
  end process;
  -- TODO: Check if this is working
  coin_bit_out <= coin_bit_out_buff;

  brach_mux: mux2_32 port map(
    first         => inc_current_pc,
    second        => branch_pc,
    select_second => branch_pc_valid,

    selected_word => selected_pc
  );

  -- clear on cpu reset
  -- first stage
  pc: reg32 port map(
    input  => selected_pc,
    output => current_pc,
    clk    => clk,
    -- we need to do this only on first clock
    ld     => coin_bit_out_buff,
    cl     => reset_cpu
  );

  inc: increment_32 port map(
    input  => current_pc,
    output => inc_current_pc,
    c      => inc_pc_c_bit
  );
  -- Branch inst. have next PC for ALU
  current_pc_out <= inc_current_pc;

  --
  -- DATA to mag
  --
  -- pass data from mag
  data2bit: std_pass32 port map(
    input  => data,
    output => data_bitv,
    pass   => working
  );
  IRreg: reg32 port map(
    input  => data_bitv,
    output => ir,
    clk    => clk,
    ld     => coin_bit_out_buff,
    cl     => reset_cpu
  );
  --
  -- ADDR to mag
  --
  adr2std: bit_pass32 port map(
    input  => selected_pc,
    output => addr,
    pass   => working
  );

  process (inc_pc_c_bit)
  begin
    if inc_pc_c_bit = '1' then
      report "Interrupt: PC address overflow." severity failure;
    end if;
  end process;

end Behavioral;
