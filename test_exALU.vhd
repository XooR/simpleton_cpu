-- TestBench Template 
library ieee;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;

ENTITY testbench IS
END testbench;

ARCHITECTURE behavior OF testbench IS

  signal t_op      : opcode;
  signal t_rs1     : word;
  signal t_rs2     : word;
  signal t_alu_out : word;
  signal t_v       : bit;
  signal t_c       : bit;
  signal t_z       : bit;
  signal t_n       : bit;

BEGIN

  -- Component Instantiation
  exALUt: exALU PORT MAP(
    op      => t_op,
    rs1     => t_rs1,
    rs2     => t_rs2,
    alu_out => t_alu_out,
    v       => t_v,
    c       => t_c,
    z       => t_z,
    n       => t_n
  );


  --  Test Bench Statements
  tb : PROCESS
    constant TICK: time := 100ms;
  BEGIN
    --
    -- Test if ADD is working fine, simple
    --

    -- op ADD
    t_op <= "000100";
    -- 122(10)
    t_rs1 <= B"0000_0000_0000_0000_0000_0000_0111_1010";
    -- 11(10)
    t_rs2 <= B"0000_0000_0000_0000_0000_0000_0000_1011";

    wait for TICK;
    report "122(10)";
    report "+ 11(10)";
    report "= 133(10), simple_add test";

    report " alu_out = " & integer'image(to_integer(signed(t_alu_out)));
    assert to_integer(unsigned(t_alu_out)) = 133
      report "122 + 11 != 133";
    assert t_v = '0'
      report "v is not 0";
    assert t_c = '0'
      report "c is not 0";
    assert t_z = '0'
      report "z is not 0";
    assert t_n = '0'
      report "n is not 0";

    --
    -- Test if overflow and c bits works
    --

    -- op ADD
    t_op <= "000100";
    -- some neg num
    t_rs1 <= B"1000_0000_0000_0000_0000_0000_0111_1010";
    -- some neg num
    t_rs2 <= B"1000_0000_0000_0000_0000_0000_0000_1011";

    wait for TICK;
    report "1000_0000_0000_0000_0000_0000_0111_1010(2)";
    report "+ 1000_0000_0000_0000_0000_0000_0000_1011(2)";
    report "= 133(10), overflow & carry test";
    
    report " alu_out = " & integer'image(to_integer(signed(t_alu_out)));

    assert t_v = '1'
      report "v is not 1";
    assert t_c = '1'
      report "c is not 1";
    assert t_z = '0'
      report "z is not 0";
    assert t_n = '0'
      report "n is not 0";

    --
    -- Test if signed ops works
    --

    -- op ADD
    t_op <= "000100";
    -- "-1"
    t_rs1 <= B"1111_1111_1111_1111_1111_1111_1111_1111";
    -- bigest positive integer
    t_rs2 <= B"0111_1111_1111_1111_1111_1111_1111_1111";

    wait for TICK;
    
    report "1111_1111_1111_1111_1111_1111_1111_1111(2) -1(10)";
    report "+ 0111_1111_1111_1111_1111_1111_1111_1111(2) INT_MAX";
    report "= 0111_1111_1111_1111_1111_1111_1111_1110(2) INT_MAX - 1";

    report " alu_out = " & integer'image(to_integer(signed(t_alu_out)));
    assert t_alu_out = B"0111_1111_1111_1111_1111_1111_1111_1110"
      report "-1 + INT_MAX != INT_MAX-1";

    assert t_v = '0'
      report "v is not 0";
    assert t_c = '1'
      report "c is not 1";
    assert t_z = '0'
      report "z is not 0";
    assert t_n = '0'
      report "n is not 0";

    wait;
  END PROCESS tb;
  --  End Test Bench 

  END;
