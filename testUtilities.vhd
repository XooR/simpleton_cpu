
library ieee;
use ieee.numeric_bit.all;
use ieee.std_logic_1164.all;
use std.textio.all;

package testUtilities is
  -- constant definitions
  constant CLK_PERIOD       : time := 100 ps;
  function to_bit_vector4s(s: string) return bit_vector;
  function bstr(slv: std_logic_vector) return string;
  -- converts a character into std_logic
  component clock_ent
  port (
    clk: out bit;
    enable_clk: in bit
    );
  end component;

end testUtilities;

package body testUtilities is
  function to_bit_vector4s(s: string) return bit_vector is 
    variable bvector : bit_vector(s'high - s'low downto 0);
    variable i       : integer;
  begin
    i := s'high - s'low;
    for j in s'range loop
      if s(j) = '1' then
        bvector(i) := '1';
      else
        bvector(i) := '0';
      end if;
      i := i - 1;
    end loop;
    return bvector;
  end to_bit_vector4s;

  function bstr(slv: std_logic_vector) return string is
    variable hex : string(1 to slv'length);
  begin
    for i in slv'range loop
      if slv(i) = '0' then
        hex(hex'length - i) := '0';
      elsif slv(i) = '1' then
        hex(hex'length - i) := '1';
      else
        hex(hex'length - i) := 'X';
      end if;
    end loop;
    return hex(1 to hex'high);
  end bstr;
end testUtilities;
