library ieee;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;

entity mux2_32 is
  port (
    first         : in word;
    second        : in word;
    select_second : in bit;

    selected_word : out word
  );
end mux2_32;

architecture Behavioral of mux2_32 is
begin

  selected_word <= second when select_second = '1' else
                   first;
end Behavioral;

