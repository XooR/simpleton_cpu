library ieee;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;

entity exALU is
  port (
    op      : in opcode;
    rs1     : in word;
    rs2     : in word;
    alu_out : out word := (others => '0');

    calc_cond: in bit;

    n       : out bit;
    z       : out bit;
    c       : out bit;
    v       : out bit
  );
end exALU;

architecture Behavioral of exALU is

begin

  process (op, rs1, rs2, calc_cond)
    variable shift: integer;
    variable result_ext: word_ext;
    variable result    : word;
    variable rs1_ext   : word_ext;
    variable rs2_ext   : word_ext;
  begin

    c <= '0';
    v <= '0';
    result_ext := (others => '0');

    shift := to_integer( unsigned(rs2) ) mod 32;
    -- Use resize, so we don't lose sign bit
    rs1_ext := '0' & rs1(rs1'left downto 0);
    rs2_ext := '0' & rs2(rs2'left downto 0);

    case op is
      -- Arithmetic
      -- OP_JSR, OP_JMP RS1 + imm -> next address
      when OP_ADD | OP_ADDI | OP_LOAD | OP_STORE | OP_BGE | OP_BGT | OP_BEQ | OP_BNQ | OP_BLT | OP_BLE | OP_JSR | OP_JMP =>
        result_ext := bit_vector( signed(rs1_ext) + signed(rs2_ext) );
        result     := result_ext(result'left downto 0);

        c <= result_ext(result_ext'left);
             -- pos + pos -> neg
        v <= (not rs1(rs1'left) and not rs2(rs2'left) and     result(result'left)) or
             -- neg + neg -> pos
             (    rs1(rs1'left) and     rs2(rs2'left) and not result(result'left));

        if calc_cond = '1' then
          result(0) := '0';
          if SIGNED(rs1) > SIGNED(rs2) and (op = OP_BGE or op = OP_BGT or op = OP_BNQ) then
            result(0) := '1';
          end if;
          if SIGNED(rs1) < SIGNED(rs2) and (op = OP_BLT or op = OP_BLE or op = OP_BNQ) then
            result(0) := '1';
          end if;
          if rs1 = rs2 and (op = OP_BGE or op = OP_BLE or op = OP_BEQ) then
            result(0) := '1';
          end if;
        end if;

--        report "rs1["
--                & integer'image(to_integer(unsigned(rs1)))
--                & "] + rs2["
--                & integer'image(to_integer(unsigned(rs2)))
--                & "] = "
--                & integer'image(to_integer(unsigned(result)))
--                ;


      when OP_SUB | OP_SUBI =>
        result_ext := bit_vector( signed(rs1_ext) - signed(rs2_ext) );

        result     := result_ext(result'left downto 0);
        -- How to calc carry?
        -- c <= result_ext'left;

             -- pos - neg -> pos + pos -> pos
        v <= (not rs1(rs1'left) and     rs2(rs2'left) and     result(result'left)) or
             -- neg - pos -> neg + neg -> neg
             (    rs1(rs1'left) and not rs2(rs2'left) and not result(result'left));

      -- Bit ops
      when OP_AND =>
        result := rs1 and rs2;
      when OP_OR =>
        result := rs1 or rs2;
      when OP_XOR =>
        result := rs1 xor rs2;
      when OP_NOT =>
        result := not rs1;

      -- Moving bits
      when OP_SHL =>
        result := bit_vector(shift_left (unsigned(rs1), shift));
      when OP_SHR =>
        result := bit_vector(shift_right(unsigned(rs1), shift));
      when OP_SAR =>
        result := bit_vector(shift_right(  signed(rs1), shift));
      when OP_ROL =>
        result := bit_vector(rotate_left(unsigned(rs1), shift));
      when OP_ROR =>
        result := bit_vector(rotate_right(unsigned(rs1), shift));

      when OP_MOV =>
        result := rs1;
      when OP_MOVI =>
        result := rs2;
--        report "MOVI "
--                & integer'image(to_integer(unsigned(result)))
--                ;
      when others =>
        result := (others => '0');
    end case;

    if to_integer(unsigned(result)) = 0 then
      z <= '1';
    else
      z <= '0';
    end if;

    n <= result(result'left);

    alu_out <= result;

  end process;
end Behavioral;

