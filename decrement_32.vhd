library ieee;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;

entity decrement_32 is
  port (
    input  : in word;
    output : out word;
    c      : out bit
  );
end decrement_32;

architecture Behavioral of decrement_32 is

begin

  process (input)
    variable input_ext : word_ext;
    variable result_ext: word_ext;
    variable result    : word;
    variable one_ext   : word_ext;
  begin
    -- PC is always unsigned
    input_ext := '0' & input(input'left downto 0);
    one_ext   := (0 => '1', others => '0');

    result_ext := bit_vector( unsigned(input_ext) - unsigned(one_ext) );       
    result     := result_ext(result'left downto 0); 

    c <= result_ext(result_ext'left);
    -- ???: we don't have overflow, only with signed operations we have it

    output <= result;
  end process;

end Behavioral;

