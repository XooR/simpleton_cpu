library ieee;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;
 
ENTITY test_registerFile IS
END test_registerFile;
 
ARCHITECTURE behavior OF test_registerFile IS 
 
    COMPONENT registerFile
    PORT(
         clk      :IN   bit;
         rs1      :IN   reg_addr;
         rs1_data :OUT  word;
         rs2      :IN   reg_addr;
         rs2_data :OUT  word;
         cl       :IN   bit;
         wr       :IN   bit;
         rwr      :IN   reg_addr;
         rwr16    :IN   bit;
         rwr_data :IN   word
        );
    END COMPONENT;
    

   --Inputs
   signal clk      :bit      := '0';
   signal rs1      :reg_addr := (others => '0');
   signal rs2      :reg_addr := (others => '0');
   signal cl       :bit      := '0';
   signal wr       :bit      := '0';
   signal rwr      :reg_addr := (others => '0');
   signal rwr16    :bit      := '0';
   signal rwr_data :word     := (others => '0');

 	--Outputs
   signal rs1_data : word;
   signal rs2_data : word;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: registerFile PORT MAP (
          clk      => clk,
          rs1      => rs1,
          rs1_data => rs1_data,
          rs2      => rs2,
          rs2_data => rs2_data,
          cl       => cl,
          wr       => wr,
          rwr      => rwr,
          rwr16    => rwr16,
          rwr_data => rwr_data
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rs1      <= B"0_0000";
      rs2      <= B"0_0000";
      rwr      <= B"0_0000";
      rwr_data <= X"0000_0000";
      wr       <= '0';
      cl       <= '1';

      wait for clk_period;

      report "Testing if all data is cleared";
      for i in 2**rs1'high downto 0 loop
        -- rs1
        rs1 <= bit_vector(to_unsigned(i, rs1'high + 1));
        wait for clk_period;
        assert to_integer(signed(rs1_data)) = 0
          report "rs1(" & integer'image(i) & ") not cleared";
        -- rs2
        rs2 <= bit_vector(to_unsigned(i, rs2'high + 1));
        wait for clk_period;
        assert to_integer(signed(rs2_data)) = 0
          report "rs2(" & integer'image(i) & ") not cleared";
      end loop;

      report "Checking if short circuit read works";
      rwr_data <= X"1111_1111";
      rwr      <= B"0_0010";
      wr       <= '1';
      rs1      <= B"0_0010";
      wait for clk_period;
      assert rs1_data = X"1111_1111"
        report "Failed to short circuit read";

      report "Check if written data is there";
      rs2 <= B"0_0010";
      rs1 <= B"0_0000"; -- should be zero
      wait for clk_period;
      assert rs2_data = X"1111_1111"
        report "Failed writing inside register file";
      assert rs1_data = X"0000_0000"
        report "Data is written that shouldn't be there";

      wait;
   end process;

END;
