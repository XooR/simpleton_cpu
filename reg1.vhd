library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.SimpletonStdPkg.all;

entity reg1 is
  port (
    input         : in bit;
    output        : out bit;
    clk           : in bit;

    -- value that is set durring clear
    default_value : in bit;

    cl            : in bit;
    ld            : in bit
  );
end reg1;

architecture Behavioral of reg1 is
  signal inner_value: bit;
begin

  process (clk)
    variable value_changed: boolean := false;
  begin
    -- read
    if rising_edge4bit(clk) then
      if cl = '1' then
        inner_value <= default_value;
      elsif ld = '1' then
        if inner_value /= input then
          value_changed := true;
        end if;

        inner_value <= input;
      end if;
    -- write to output
    elsif falling_edge4bit(clk) then
      output <= inner_value;
      if value_changed then
        report bit'image(inner_value);
        value_changed := false;
      end if;
    end if;
      
  end process;
end Behavioral;

