library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;
use work.testUtilities.all;

ENTITY TestCPU_jsr IS
END TestCPU_jsr;

ARCHITECTURE behavior OF TestCPU_jsr IS

  --Inputs
  signal data             :  sword := (others => 'Z');
  signal clk              :  bit := '0';
  signal reset_cpu        :  bit := '0';

  --Outputs
  signal addr            :  sword := (others => 'Z');
  signal r               :  std_logic := 'Z';
  signal w               :  std_logic := 'Z';

  -- Memory
  constant MEM_SIZE : integer := 300;
  type TMem is array(0 to MEM_SIZE - 1) of sword;
  constant TESTPATH                : string := "simplest";
  constant mem_file                : string := "./test_confs/asm_jsr_mem.txt";
  signal Mem                       : TMem := (others => "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
  file fmem                        : text is mem_file;
  signal halt                      : bit := '0'; -- kills memory process
  signal memory_file_read_finished : bit := '0';

BEGIN
  init : process
    -- variable declarations
    variable ln     : line;
    variable saddr  : sword;
    variable data_tmp: sword;
    variable sdata  : string(1 to WORD_LEN);
    variable blank  : string(1 to 1);
    variable adr    : integer;
  begin
    -- code that executes only once
    -- load instruction memory
    while not endfile(fmem) loop
      readline(fmem, ln);
      ieee.std_logic_textio.hread(ln, saddr);
      read(ln, blank);
      read(ln, sdata);
      adr := to_integer(ieee.numeric_std.unsigned(saddr));
      report "      ==  saddr  " & integer'image(adr);
      report "      ==  sdata  " & sdata;
      data_tmp := To_StdLogicVector(to_bit_vector4s(sdata));
      Mem(adr) <= data_tmp;
    end loop;
    memory_file_read_finished <= '1';
    wait;
  end process init;
 -- Clock process definitions
  Clock_inst : clock_ent port map (
    clk        => clk,
    enable_clk => '1'
  );
  CPU_inst : CPU port map (
    -- list connections between master ports and signals
    clk => clk,
    reset => reset_cpu,
    halt  => halt,
    r     => r,
    w     => w,
    addr  => addr,
    data  => data
  );


   -- Stimulus process
  stim_proc: process
  begin
    wait until memory_file_read_finished = '1';

    wait for clk_period / 100;

    -- init if stage
    reset_cpu <= '1';
    wait for clk_period;
    reset_cpu <= '0';

    -- * - instructions that should be executed
    --* MOVI R0, #0
    --* JSR R0, #3
    --HALT
    --* MOVI R1, #32
    --* STORE R0, R1, #100
    --* RTS
    wait for clk_period*25;

    assert Mem(100) = B"0000_0000_0000_0000_0000_0000_0010_0000"
      report "Mem(100) == 32" severity failure;
    report "ALL tests okey" severity failure;

    wait;
  end process;

  mem_process : process(clk, halt)
    variable writeMem : boolean := false;
    variable readMem  : boolean := false;

    variable data_tmp : sword := (others => 'Z');
    variable addr_int : integer;
  begin
    -- code executes for every event on sensitivity list
    report "Testing...";
    -- main part

    if falling_edge4bit(clk) then
      if not (writeMem or readMem) then
        data <= (others => 'Z');
      end if;

      if readMem then
        data    <= Mem(addr_int);
        readMem := false;
        report "Finished reading, data is: "
          & integer'image(to_integer(ieee.numeric_std.unsigned(data)))
          ;
      end if;
      if writeMem then
        Mem(addr_int) <= data_tmp;
        writeMem := false;
        report "Finished writing";
      end if;
    end if;

    if rising_edge4bit(clk) then
      if r = '1' then
        readMem  := true;
        addr_int := to_integer(ieee.numeric_std.unsigned(addr));
        report "Begining reading memory from address: "
          & integer'image(to_integer(ieee.numeric_std.unsigned(addr)));
      end if;
      if w = '1' then
        writeMem := true;
        addr_int := to_integer(ieee.numeric_std.unsigned(addr));
        report "Begining writing to memory on address: "
          & integer'image(to_integer(ieee.numeric_std.unsigned(addr)))
          & ", data: "
          & integer'image(to_integer(ieee.numeric_std.unsigned(data)))
          ;
        data_tmp := data;
      end if;
    end if;

    if halt = '1' then
      assert false report "Testing done." severity failure;
    end if;
  end process mem_process;

END;

