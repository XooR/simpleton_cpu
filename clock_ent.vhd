
library ieee;
use ieee.numeric_bit.all;
use work.testUtilities.all;

entity clock_ent is
  port (
    clk: out bit;
    enable_clk: in bit
    );
end clock_ent;

architecture Behavioral of clock_ent is

begin
   clk_process: process
   begin
    if enable_clk = '1' then
      clk <= '0';
      wait for CLK_PERIOD/2;
      clk <= '1';
      wait for CLK_PERIOD/2;
    else
      clk <= '0';
      wait for CLK_PERIOD;
    end if;
   end process;
end Behavioral;

