library ieee;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;

entity spReg32 is
  port (
    input  : in word;
    output : out word;
    clk    : in bit;
    inc    : in bit;
    dec    : in bit;
    ld     : in bit;
    cl     : in bit
   );
end spReg32;

architecture Behavioral of spReg32 is
  signal inner_value: word;
  constant one: bit_vector(31 downto 0) := (0 => '1', others => '0');
begin

  process (clk)
    variable value_changed: boolean;
  begin
    -- read
    if rising_edge4bit(clk) then
      if cl = '1' then
        inner_value <= (others => '0');
      elsif ld = '1' then

        if inner_value /= input then
          value_changed := true;
        end if;

        inner_value <= input;

      elsif inc = '1' then
        inner_value <= bit_vector(unsigned(inner_value) + unsigned(one));
        value_changed := true;
      elsif dec = '1' then
        inner_value <= bit_vector(unsigned(inner_value) - unsigned(one));
        value_changed := true;
      end if;
    -- write to output
    elsif falling_edge4bit(clk) then
      output <= inner_value;

      if value_changed then
        value_changed := false;
        report vec2str(inner_value);
      end if;
    end if;

  end process;

end Behavioral;
