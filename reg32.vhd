library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.SimpletonStdPkg.all;

entity reg32 is
  port (
    input  : in word;
    output : out word;
    clk    : in bit;
    ld     : in bit;
    cl     : in bit
   );
end reg32;

architecture Behavioral of reg32 is
  signal inner_value: word;
begin

  process (clk)
    variable value_changed: boolean;
  begin
    -- read
    if rising_edge4bit(clk) then
      if cl = '1' then
        inner_value <= (others => '0');
      elsif ld = '1' then

        if inner_value /= input then
          value_changed := true;
        end if;

        inner_value <= input;
      end if;
    -- write to output
    elsif falling_edge4bit(clk) then
      output <= inner_value;

      if value_changed then
        value_changed := false;
        report vec2str(inner_value);
      end if;
    end if;

  end process;

end Behavioral;

