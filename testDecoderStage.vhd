library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;
use work.testUtilities.all;

ENTITY test_decoderStage IS
END test_decoderStage;
 
ARCHITECTURE behavior OF test_decoderStage IS 
 
  --Inputs
  signal data             :  sword := (others => 'Z');
  signal clk              :  bit := '0';
  signal branch_pc        :  word := (others => '0');
  signal branch_pc_valid  :  bit := '1';
  signal reset_cpu        :  bit := '0';
  signal start_cpu        :  bit := '0';

  --Outputs
  signal addr            :  sword := (others => 'Z');
  signal r               :  std_logic := 'Z';
  signal w               :  std_logic := 'Z';
  signal ir              :  word := (others => '0');
  signal coin_bit_if_in  :  bit := '0';
  signal coin_bit_if_out :  bit := '0';
  signal current_pc_out  :  word;

  -- Decoder stage
  signal rs1      : word;
  signal rs2      : word;
  signal opc      : opcode;
  signal imm16    : imm16t;
  signal wr       : bit := '0';
  signal rwr16    : bit;
  signal rwr_data : word;

  -- Memory
  constant MEM_SIZE : integer := 300;
  type TMem is array(0 to MEM_SIZE - 1) of sword;
  constant TESTPATH                : string := "simplest";
  constant mem_file                : string := "./test_confs/" & TESTPATH & "/asm_mem.txt";
  signal Mem                       : TMem := (others => "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
  file fmem                        : text is mem_file;
  signal halt                      : bit := '0'; -- kills memory process
  signal memory_file_read_finished : bit := '0';

  -- instructions that we execute
  constant INST_1 : string := "MOVI   R1, #43";
  constant INST_2 : string := "MOVI   R1, #64";
  constant INST_3 : string := "ADD    R0, R1, R2";
  constant INST_4 : string := "BGE    R0, R1, #28";
  constant INST_5 : string := "HALT";
BEGIN
  init : process
    -- variable declarations  
    variable ln     : line;
    variable saddr  : sword;
    variable data_tmp: sword;
    variable sdata  : string(1 to WORD_LEN);
    variable blank  : string(1 to 1);
    variable adr    : integer;
  begin
    -- code that executes only once
    -- load instruction memory
    while not endfile(fmem) loop
      readline(fmem, ln);
      ieee.std_logic_textio.hread(ln, saddr);
      read(ln, blank);
      read(ln, sdata);
      adr := to_integer(ieee.numeric_std.unsigned(saddr));
      report "      ==  saddr  " & integer'image(adr);
      report "      ==  sdata  " & sdata;
      data_tmp := To_StdLogicVector(to_bit_vector4s(sdata));
      Mem(adr) <= data_tmp;
    end loop;
    memory_file_read_finished <= '1';
    wait;
  end process init;
	-- Instantiate the Unit Under Test (UUT)
  i_ifStage: ifStage PORT MAP (
    addr            => addr,
    data            => data,
    r               => r,
    w               => w,
    clk             => clk,
    ir              => ir,
    coin_bit_in     => coin_bit_if_in,
    coin_bit_out    => coin_bit_if_out,
    branch_pc       => branch_pc,
    branch_pc_valid => branch_pc_valid,
    current_pc_out  => current_pc_out,
    reset_cpu       => reset_cpu
  );
  i_decoderStage: decoderStage PORT MAP (
      clk          => clk,
      ir           => ir,
      rs1          => rs1,
      rs2          => rs2,
      opc          => opc,
      imm16        => imm16,
      coin_bit_in  => coin_bit_if_out,
      coin_bit_out => coin_bit_if_in,
      reset_cpu    => reset_cpu,
      wr           => wr,
      rwr16        => rwr16,
      rwr_data     => rwr_data,
      halt         => halt
  );

 -- Clock process definitions
  Clock_inst : clock_ent port map (
    clk        => clk,
    enable_clk => '1'
  );
  
 
   -- Stimulus process
   stim_proc: process
   begin
     wait until memory_file_read_finished = '1';

     -- init if stage
     reset_cpu <= '1';
     wait for clk_period;
     reset_cpu <= '0';

     wait for clk_period*2;
     branch_pc_valid <= '0';
     wait for clk_period / 100;


     wait for clk_period;
-- MOVI   R1, #43
     if opc /= B"101000" then
       assert false report "OPC is not read correctly " & INST_1;
     else
       report "OPC is read correctly " & INST_1;
     end if;
     if imm16 /= B"0000_0000_0010_1011" then
       assert false report "imm16 is not read correctly " & INST_1;
     else
       report "imm16 is read correctly " & INST_1;
     end if;

     wait for clk_period * 3;
-- MOVI   R1, #64
     if opc /= B"101000" then
       assert false report "OPC is not read correctly " & INST_2;
     else
       report "OPC is read correctly " & INST_2;
     end if;
     if imm16 /= B"0000_0000_0100_0000" then
       assert false report "imm16 is not read correctly " & INST_2;
     else
       report "imm16 is read correctly " & INST_2;
     end if;

     wait for clk_period * 3;
-- ADD    R0, R1, R2
     if opc /= B"000100" then
       assert false report "OPC is not read correctly " & INST_3;
     else
       report "OPC is read correctly " & INST_3;
     end if;
-- HALT

     wait;
   end process;

  mem_process : process(clk, halt)
    variable writeMem : boolean := false;
    variable readMem  : boolean := false;
    
    variable data_tmp : sword := (others => 'Z');
    variable addr_int : integer;
  begin
    -- code executes for every event on sensitivity list
    report "Testing...";
    -- main part
    
    if falling_edge4bit(clk) then
      if not (writeMem or readMem) then
        data <= (others => 'Z');
      end if;
      
      if readMem then
        data    <= Mem(addr_int);
        readMem := false;
        report "Finished reading, data is: "
          & integer'image(to_integer(ieee.numeric_std.unsigned(data)))
          ;
      end if;
      if writeMem then
        Mem(addr_int) <= data_tmp;
        writeMem := false;
        report "Finished writing";
      end if;
    end if;

    if rising_edge4bit(clk) then
      if r = '1' then
        readMem  := true;
        addr_int := to_integer(ieee.numeric_std.unsigned(addr));
        report "Begining reading memory from address: "
          & integer'image(to_integer(ieee.numeric_std.unsigned(addr)));
      end if;
      if w = '1' then
        writeMem := true;
        addr_int := to_integer(ieee.numeric_std.unsigned(addr));
        report "Begining writing to memory on address: "
          & integer'image(to_integer(ieee.numeric_std.unsigned(addr)))
          & ", data: "
          & integer'image(to_integer(ieee.numeric_std.unsigned(data)))
          ;
        data_tmp := data;
      end if;
    end if;
    
    if halt = '1' then
      assert false report "Testing done." severity failure;
    end if;
  end process mem_process;

END;
