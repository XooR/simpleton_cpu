library ieee;
use ieee.numeric_bit.all;

use work.SimpletonStdPkg.all;

entity registerFile is
  port (
    clk: in  bit;

    -- Only reads data when this bit is set to 1, after that value is still
    read_allowed: in bit;

    rs1      : in  reg_addr;
    rs1_data : out word;

    rs2      : in  reg_addr;
    rs2_data : out word;

	  cl       : in  bit;

    wr       : in  bit;
    rwr      : in  reg_addr;
    -- write only lower 16bits
	  rwr16    : in  bit;
    rwr_data : in  word
  );
end registerFile;

architecture behavioral of registerFile is
  type   registers_t is array(0 to 31) of word;
  signal registers : registers_t := (others => "00000000000000000000000000000000");

  -- shortcut get_register, even if it is not written, not needed here
  impure function get_register(rs: reg_addr) return word is
    variable result: word;
  begin
    -- we shortcut data we are writing to read it
    if (wr = '1' and rs = rwr and rwr16 /= '1') then
      result := rwr_data;
    -- same thing, but when we want only to write lower 16bits
    elsif (wr = '1' and rs = rwr and rwr16 = '1') then
      result := registers(to_integer(unsigned(rwr)))(rwr_data'high downto 16)
                & rwr_data(15 downto 0);
    -- we do ordinary read
    else
      result := registers(to_integer(unsigned(rs)));
    end if;

    return result;
  end get_register;
begin
  process(clk) is
    variable tmp_rs1_data : word;
    variable tmp_rs2_data : word;
  begin
    -- write
    if (falling_edge(clk)) then
      -- clear
      if (cl = '1') then
        for i in 0 to 31 loop
          registers(i) <= (others => '0');
        end loop;
      -- write
      elsif (wr = '1') then
        if(rwr16 = '1') then
          registers(to_integer(unsigned(rwr)))(15 downto 0) <= rwr_data(15 downto 0);
        else
          registers(to_integer(unsigned(rwr))) <= rwr_data;
        end if;
      end if;
    elsif (rising_edge(clk)) then
      if read_allowed = '1' then
        -- read
        tmp_rs1_data := get_register(rs1);
        tmp_rs2_data := get_register(rs2);
        rs1_data     <= tmp_rs1_data;
        rs2_data     <= tmp_rs2_data;
      end if;
    end if;
  end process;
end architecture behavioral;

